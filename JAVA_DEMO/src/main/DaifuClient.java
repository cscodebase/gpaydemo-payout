package main;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Map;
import java.util.Map.Entry;
import java.util.SortedMap;

import org.apache.http.client.fluent.Request;
import org.apache.http.entity.ContentType;

import params.DaifuRequest;

public class DaifuClient {
	public static String key = "";
	
	
	public String toQueryString(Map<String, String> data)
			throws UnsupportedEncodingException {
		StringBuilder sb = new StringBuilder();
		for (Entry<String, String> entry : data.entrySet()) {
			sb.append(entry.getKey());
			sb.append("=");
			sb.append(entry.getValue());
			sb.append("&");
		}
		sb.setLength(sb.length() - 1);
		String ret = sb.toString();
		return ret;
	}
	
	public String daifuPost(DaifuRequest request) throws Exception {
		
		SortedMap<String, String> params = request.getParams();
		String sign = request.sign("daifu-request", params, DaifuClient.key);
		
		params.put("sign", sign);
		params.put("enc_data", URLEncoder.encode(request.enc_data, "UTF-8"));
		
		return this.sendDaifuRequest(this.toQueryString(params));
	}
	
	/**
	 * 
	 * @param name 姓名
	 * @param cardNo 卡号
	 * @param idNo 身份证号
	 * @return
	 */
	protected String getEncData(String name, String cardNo, String idNo) {
		String encData = name + "|" + cardNo + "|" + idNo;
		return encData;
	}

	/**
	 * 发起代付请求。
	 * 
	 * @param DaifuJinmao
	 *            代付
	 * @param upstream
	 * @return
	 * @throws Exception
	 */
	protected String sendDaifuRequest(String data)
			throws Exception {
		String url = "https://gapi.llque.com/api/v2/daifu/request";
//		String url = "http://localhost:8777/api/v2/daifu/request";
		String res;
		try {
			
			res = Request.Post(url)
					.addHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8")
					.bodyString(data, ContentType.APPLICATION_FORM_URLENCODED).connectTimeout(100000)
					.socketTimeout(100000).execute().returnContent().asString();
			
			System.out.println("Daifu request: " + data);
		} catch (Exception e) {
			System.out.println("Send failed request, url: " + url + ", body: " + data);
			e.printStackTrace();
			throw e;
		}

		return res;
	}
	
	public static void main(String[] args) throws Exception {
		DaifuClient client = new DaifuClient();
		DaifuRequest request = new DaifuRequest();
		
		request.amount = "10";
		request.bank_code = "1";
		request.company_id = "fb";
		request.merchant_order_no = String.valueOf(System.currentTimeMillis());
		request.phone= "15678895623";
		
		// 银行卡持卡人姓名
		String name = "张三";
		// 银行卡持卡人姓名
		String cardNo = "6512436514256341265";
		// 银行卡持卡人姓名
		String idNo = "420322198605061122";
				
		request.enc_data = DaifuRequest.encrypt(client.getEncData(name, cardNo, idNo), DaifuClient.key);
		
		String res = client.daifuPost(request);
		System.out.println("Daifu response: " + res);
	}
}
